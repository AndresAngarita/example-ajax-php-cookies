<?php

if ($_GET) {
    print_r((count($_COOKIE) > 0 && isset($_COOKIE['vehicles']))?(json_encode(json_decode($_COOKIE['vehicles'],true))):0);
}

if ($_POST) {
    if ($_POST['action']=='send') {
    
        print_r(SetDataInCookie());
    
    } else if ($_POST['action']=='delete') {
    
        print_r(DeleteCookie());
    
    }
}


function SetDataInCookie(){
    if(count($_COOKIE) > 0 && isset($_COOKIE['vehicles'])){
        $vehicles = json_decode($_COOKIE['vehicles'],true);
        
        if (count($vehicles)==1 && isset($vehicles[0]['Marca'])) {
            array_push($vehicles,$_POST['data']);
            $vehicles = "[".json_encode($vehicles)."]";
        }else {
            array_push($vehicles[0],$_POST['data']);
            $vehicles = "[".json_encode($vehicles[0])."]";
        }
        
        try {
            setcookie('vehicles', $vehicles, time()+3600, "/");
            return true;
        } catch (Exception $e) {
            return false;
        }
        
    }else{
        try {
            $vehicle = "[".json_encode($_POST['data'])."]";
            setcookie('vehicles', $vehicle, time()+3600, "/"); 
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}

function DeleteCookie(){
    try {
        setcookie('vehicles', "", time()+3600, "/");
        return true;
    } catch (Exception $e) {
        return false;
    }
}