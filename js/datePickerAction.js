$( function() {
    $( "#datepicker" ).datepicker({ 
       yearRange: "c-100:c",
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'yy',
        onClose: function(dateText, inst) { 
        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
        $(this).datepicker('setDate', new Date(year, 1));
    } 
    });
    
} );

