var logicJS = {
    LoadTable: function () {
        
        $.get('php/Persist.php', { load: 'table'}, function (res) {
            
            if (res==0) {
                $("#tableVehicle").html("<tr><td colspan='4' class = 'text-center'>No hay vehiculos registrados</td></tr>");    
            }else{
                var html = "";
                res = JSON.parse(res);
                
                if (res.length==1 && res[0]['Marca']) {
                    
                    html += "<tr>";
                    for (dato in res[0]) {
                        if (dato == 'Color') {
                            html += "<td style = 'padding: 10px 0px 10px 20px'><div style='background-color:"+res[0][dato]+";width:20px;height:20px;border-radius: 15%;'>&nbsp;</div></td>";    
                        }else{
                            html += "<td>"+res[0][dato]+"</td>";
                        }
                    }
                    html += "</tr>";
                    
                }else{
                    res[0].forEach(registro => {
                        html += "<tr>";
                        for (dato in registro) {
                            if (dato == 'Color') {
                                html += "<td style = 'padding: 10px 0px 10px 20px'><div style='background-color:"+registro[dato]+";width:20px;height:20px;border-radius: 15%'>&nbsp;</div></td>";    
                            }else{
                                html += "<td>"+registro[dato]+"</td>"; 
                            }
                        }
                        html += "</tr>";
                    });
                }
                

                $("#tableVehicle").html(html);
            }
        });
    },
    
    SendData:function(){
        var controls = logicJS.ValidateFields();
        
        if (controls) {
            $.post('php/Persist.php', {action:'send', data: controls}, function (res) {
                if (res==1) {
                    Swal.fire(
                        'Bien!',
                        'Datos agregados con éxito!',
                        'success'
                    );

                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        html: 'Ocurrió un error inesperado al cargar los datos, por favor intenta mas tarde.',
                    }); 
                }
            }).done(function() {
                console.log("Done!");
                logicJS.LoadTable();
            });


            
        }
    },

    DeleteData:function(){
        Swal.fire({
            title: 'Estas seguro?',
            text: "No se podran recuperar los datos!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, borrar!'
          }).then((result) => {
            if (result.isConfirmed) {
                $.post('php/Persist.php', { action: 'delete'}, function (res) {
                    if (res==1) {
                        Swal.fire(
                            'Bien!',
                            'Datos eliminados con éxito!',
                            'success'
                        );
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            html: 'Ocurrió un error inesperado al eliminar los datos, por favor intenta mas tarde.',
                        }); 
                    }
                }).done(function() {
                    console.log("Done!");
                    logicJS.LoadTable();
                });
            }
          })
    },

    GetDataFromControls: function (){
        var controls = {
            'Marca' : $("#brand-input").val(),
            'Color' : $("#color-input").val(),
            'Precio' : $("#price-input").val(),
            'Año'  : $("#datepicker").val(),
        };
        return controls;
    },
   
    ClearDataFromControls:function(){
        $("#brand-input").val("");
        $("#color-input").val("");
        $("#price-input").val("");
        $("#datepicker").val("");
    },

    ValidateFields:function(){
        var emptys = [];
        var controls = logicJS.GetDataFromControls();
        
        for(element in controls) {
            if (typeof controls[element] === 'undefined' || controls[element] == "") {
                emptys.push(element);
            }
        }; 
        
        if (emptys.length>0) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                html: 'Parece que te falto por llenar los siguientes campos: '+emptys.join(", ")+'.<br> Por favor regresa y llena los que hacen falta para continuar.',
            });
            return false;
        }else{
            logicJS.ClearDataFromControls();
            return controls;
        }
    }
};

window.onload = logicJS.LoadTable;
