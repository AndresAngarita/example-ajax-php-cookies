<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="imgs/carIcon.png"/>
    
    <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="lib/jquery-ui-1.12.1/jquery-ui.css">
    <link rel="stylesheet" href="lib/jquery-ui-1.12.1/jquery-ui.structure.css">
    <link rel="stylesheet" href="lib/jquery-ui-1.12.1/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <title>EngagementForm</title>

</head>
<body>
    <header class="bg-dark text-center">
        <div class="text-white h2 align-content-between">
            <p>Datos Automóvil</p> 
        </div>
    </header>
    <div class="alert alert-primary alert-dismissible fade show" role="alert">
        Los datos enviados, se guardarán temporalmete, despues de <strong>1 Hora</strong> seran borrados, 
        para que no queden en la memoria de tu pc.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="container">
        <div class="row">

            <div class="col-5">
                <!------------------ form  ---------------- -->
                <div class="h2">Infomación automovil</div>
                <div style="margin-top: 30px;"></div>
                <div class="form-group row">
                    <label for="brand-input" class="col-2 col-form-label">Marca</label>
                    <div class="col-10">
                        <input class="form-control" type="text" id="brand-input" placeholder="marca del vehiculo" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="color-input" class="col-2 col-form-label">Color</label>
                    <div class="col-10">
                        <input class="form-control" type="color" value="#b90000" id="color-input" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="price-input" class="col-2 col-form-label">Precio</label>
                    <div class="col-10">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input type="text" class="form-control" id="price-input" aria-label="Cantidad (En pesos)" placeholder="precio del vehiculo" required>
                            <div class="input-group-append">
                                <span class="input-group-text">.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date-input" class="col-2 col-form-label">Año</label>
                    <div class="col-10">
                        <input class="form-control" type="text" id="datepicker" placeholder="año" required>    
                    </div>
                </div>   
                <div style="margin-top: 40px;"></div>
                <div class="form-group row">
                    <div class="col-5">
                        <button class="btn btn-success" id="enviarBtn" onclick="logicJS.SendData();">Enviar</button>
                    </div>
                    <div class="col-7 text-right">
                        <button class="btn btn-danger " onclick="logicJS.DeleteData();">Borrar Todo</button>
                    </div>
                </div>
                <!-------------- form ------------------------>
            </div>
            
            <div class="col-7" id="contTable">
                <!-------------------- table ----------------->
                <div class="h2">Automóviles</div>
                <div style="margin-top: 30px;"></div>
                <table class="table table-dark table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Marca</th>
                            <th>Color</th>
                            <th>Precio</th>
                            <th>Año</th>
                        </tr>
                    </thead>
                    <tbody id="tableVehicle">
                        <!-- dataOfTable -->
                    </tbody>
                </table>
                <!-------------------- table ----------------->
            </div>
        </div>
        
    </div>

    <footer class="page-footer font-small blue bg-dark fixed-bottom">
        <!-- Copyright -->
        <div class="footer-copyright text-center text-white py-3">© 2020 Made by : Andrés F. Angarita E.</div>
        <!-- Copyright -->
    </footer>

   
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    <script src="lib/jquery-ui-1.12.1/external/jquery/jquery.js"></script>
    <script src="lib/jquery-ui-1.12.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    
    <script src="js/datePickerAction.js"></script>
    <script src="js/logicAjax.js"></script>
</body>
</html>