# Basic example ajax php cookies

This example was develop by Andres Angarita/[RewDev](https://rewdev.net/ "Website"). This example show how to integrate html5, ajax, php and save data in cookies.

## Description

---

Consist in a form with 4 fields: marca, color, precio and año.

![Fields](imgs/fields.png)

With a button that load in a table the new registry of a car.

![buttonSend](imgs/buttonSend.png)

Also contains a button that delete all data of table.

![buttonDelete](imgs/buttonDelete.png)

The table is a basic boostrap table with the data of cars.

![table](imgs/table.png)

All persistence is manage with cookies, those cookies are clean 1 hour after that the last registry are loaded. The next code show how the cookie is load.

``` php
setcookie('vehicles', $vehicle, time()+3600, "/");
```

The 4 parameters are:

- *'vehicles'* -> that correspond to name of cookie.
- *$vehicle* -> that correspond to value to save in the cookie.
- *time()+3600* -> that correspond to current time plus 3600 seconds.
- *"/"* -> that correspond to path of save the cookie in the server.

For more information go to documentation of [setcookie()](https://www.php.net/manual/es/function.setcookie.php "Php Documentation") function.

## use

---

To use feel free to clone or download the project, set in you server and use for you need.
